cmake_minimum_required(VERSION 3.5)
project(vfs.libtorrent)

list(APPEND CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR})
# Libtorrent's CMake files are installed in "share/cmake/Modules"
list(APPEND CMAKE_MODULE_PATH ${CMAKE_PREFIX_PATH}/share/cmake/Modules)

find_package(Kodi REQUIRED)
find_package(ZLIB REQUIRED)
find_package(OpenSSL REQUIRED)
find_package(Boost REQUIRED COMPONENTS system date_time chrono random)
find_package(LibtorrentRasterbar REQUIRED)

include_directories(${KODI_INCLUDE_DIR}/..
                    ${LibtorrentRasterbar_INCLUDE_DIRS}
                    ${OPENSSL_INCLUDE_DIR}
                    ${ZLIB_INCLUDE_DIRS}
                    ${Boost_INCLUDE_DIRS})

set(TORRENT_SOURCES src/TorrentFile.cpp)

set(DEPLIBS ${OPENSSL_LIBRARIES}
            ${ZLIB_LIBRARIES}
#             ${LibtorrentRasterbar_LIBRARIES}
            ${CMAKE_PREFIX_PATH}/lib/libtorrent-rasterbar.a
#             ${Boost_LIBRARIES}
            ${CMAKE_PREFIX_PATH}/lib/libboost_chrono.a
            ${CMAKE_PREFIX_PATH}/lib/libboost_date_time.a
            ${CMAKE_PREFIX_PATH}/lib/libboost_random.a
            ${CMAKE_PREFIX_PATH}/lib/libboost_system.a)
# When LibtorrentRasterbar is imported with "find_package()" the following
# problems occur:
#  - LibtorrentRasterbar_LIBRARIES variable contains "LibtorrentRasterbar_LIBRARIES-NOTFOUND"
# instead of the path to libtorrent-rasterbar.a because IMPORTED_LOCATION does
# not exists (only IMPORTED_LOCATION_DEBUG or IMPORTED_LOCATION_RELEASE exists)
#  - Boost_LIBRARIES is overwritten and contains only libboost_system.a so the
# remaining boost libraries are missing
# Because of this the path to the libtorrent and boost libraries are added
# manually.

build_addon(vfs.libtorrent TORRENT DEPLIBS)

include(CPack)