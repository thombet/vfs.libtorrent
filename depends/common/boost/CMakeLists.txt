cmake_minimum_required(VERSION 3.5)
project(boost)

if(CMAKE_SYSTEM_NAME STREQUAL Windows)
    list(APPEND configure_command bootstrap.bat)
elseif(CMAKE_SYSTEM_NAME STREQUAL Darwin)
    # Same bootstrap command for Linux and macOS
    list(APPEND configure_command ./bootstrap.sh --prefix=${OUTPUT_DIR})
endif()


if(CMAKE_SYSTEM_NAME STREQUAL Windows)
    set(B2_TARGET_OS windows)
elseif(CMAKE_SYSTEM_NAME STREQUAL Darwin)
    set(B2_TARGET_OS darwin)
elseif(CMAKE_SYSTEM_NAME STREQUAL Linux)
    set(B2_TARGET_OS linux)
endif()
# https://www.boost.org/doc/libs/1_64_0/doc/html/bbv2/reference.html#bbv2.overview.builtins.features


list(APPEND build_command ./b2 link=static variant=release threading=multi)
list(APPEND build_command optimization=space target-os=${B2_TARGET_OS})
list(APPEND build_command --prefix=${OUTPUT_DIR})
list(APPEND build_command --with-date_time)
list(APPEND build_command --with-system)
list(APPEND build_command --with-chrono)
list(APPEND build_command --with-random)
list(APPEND build_command install)

include(ExternalProject)

externalproject_add(boost
                    SOURCE_DIR ${CMAKE_SOURCE_DIR}
                    UPDATE_COMMAND ""
                    CONFIGURE_COMMAND ${configure_command}
                    BUILD_IN_SOURCE 1
                    BUILD_COMMAND ${build_command}
                    INSTALL_COMMAND "")

# Force the creation of a dummy installation target
install(CODE "MESSAGE(\"Nothing to install\")")
