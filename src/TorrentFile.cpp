/*
 *  Copyright (C) 2021 Thomas Bétous
 *
 *  SPDX-License-Identifier: GPL-3.0-only
 *  See LICENSE.md for more information.
 */
#include <thread>

#include <kodi/addon-instance/VFS.h>
#include <kodi/Filesystem.h>
#include <kodi/General.h>
#include <kodi/gui/dialogs/Progress.h>

#include "libtorrent/add_torrent_params.hpp"
#include "libtorrent/alert.hpp"
#include "libtorrent/error_code.hpp"
#include "libtorrent/file_storage.hpp"
#include "libtorrent/session.hpp"
#include "libtorrent/sha1_hash.hpp"
#include "libtorrent/torrent_handle.hpp"
#include "libtorrent/torrent_info.hpp"
#include "libtorrent/torrent_status.hpp"

namespace lt = libtorrent;

// Structure used to share information between APIs
struct TorrentContext
{
  std::string info_json; // Information about the torrent stored as JSON
  size_t read_data_length; // Amount of data read by the Read() API.
  lt::sha1_hash info_hash; // Info-hash of the torrent
};

void log_function_name(std::string function_name)
{
  kodi::Log(ADDON_LOG_DEBUG, "In the function \"%s()\"", function_name.c_str());
}

//------------------------------------------------------------------------------

class CTorrentFile : public kodi::addon::CInstanceVFS
{
public:

  //--------------------------------- Members ----------------------------------

  // Libtorrent session that will be reused for all the downloads
  lt::session * lt_session = nullptr;

  // Path to the folder that will contain all the downloaded files
  std::string download_folder = kodi::vfs::TranslateSpecialProtocol("special://temp/vfs.libtorrent");

  //--------------------------------- Functions --------------------------------

  // Constructor
  CTorrentFile(KODI_HANDLE instance) : CInstanceVFS(instance)
  {
    kodi::Log(ADDON_LOG_DEBUG, "Instantiation of CTorrentFile");
  }

  // Destructor
  ~CTorrentFile()
  {
    kodi::Log(ADDON_LOG_DEBUG, "Destructing the instance of CTorrentFile");

    if (kodi::GetSettingBoolean("delete_download_folder"))
    {
      kodi::Log(ADDON_LOG_INFO, "Deleting the temporary folder %s", download_folder.c_str());
      if (!kodi::vfs::RemoveDirectory(download_folder))
      {
        kodi::Log(ADDON_LOG_ERROR, "An error occurred when deleting the folder %s", download_folder.c_str());
        // On macOS with Kodi 18.x, an error is reported where there are hidden
        // files in the folder: the folder is not deleted but the non-hidden
        // files are.
      }
    }

    // Before deleting the libtorrent session, terminate it properly by removing
    // all the torrents (but keep the downloaded files)
    for (lt::torrent_handle handle : lt_session->get_torrents())
    {
      lt_session->remove_torrent(handle);
    }

    // Clean up objects
    delete lt_session;
  }

  // This function will add the torrent and download the metadata if missing but
  // it will not download any file of the torrent.
  void* Open(const VFSURL& url) override
  {
    log_function_name(__FUNCTION__);
    kodi::Log(ADDON_LOG_DEBUG, "\nurl=%s \nfilename=%s \nhostname=%s", url.url, url.filename, url.hostname);

    // Initiliaze the libtorrent session
    if (lt_session == nullptr)
    {
      initialize_session();
    }

    // Initialize the context structure
    TorrentContext* torrent_context = new TorrentContext;
    torrent_context->read_data_length = 0;
    torrent_context->info_json = "{}";

    // Configure the torrent:
    lt::add_torrent_params params;
    //   - Define the path where the file will be downloaded
    params.save_path = download_folder;
    //   - Set the URL of the torrent file
    params.url = url.hostname;
    //   - Enable the sequential mode i.e. download the pieces in order so that
    //     the playback can be started while the download is still in progress.
    //     (It will have to be improved to support better when the playback does
    //     not start from the beginning.)
    params.flags |= lt::add_torrent_params::flag_sequential_download;
    //   - Set all the file priorities to 0 (dont_download) so that only the
    //     metadata are downloaded. (The vector used for initializing the
    //     has a size of 100 which should be enough to cover most of the cases.)
    std::vector<boost::uint8_t> priorities(100, 0);
    params.file_priorities = priorities;

    // Add the torrent to the session
    lt::torrent_handle handle = lt_session->add_torrent(params);

    kodi::Log(ADDON_LOG_DEBUG, "\nChecking torrent's metadata...");

    // If the torrent does not have metadata, download it. All the alerts will
    // be logged with debug level in the process which may be useful to debug
    // in case something does not work. If the metadata takes more than 30
    // seconds to be downloaded, the function will return with an error.
    std::vector<lt::alert*> alerts;
    int64_t timeout = 0;
    lt::torrent_status status = handle.status();
    while (status.has_metadata == false)
    {
      status = handle.status();

      lt_session->pop_alerts(&alerts);
      for (lt::alert* a : alerts) {
        kodi::Log(ADDON_LOG_DEBUG, "alert:\n%s", a->message().c_str());
      }

      std::this_thread::sleep_for(std::chrono::milliseconds(100));

      if (++timeout > 300)
      {
        // We could not retrieve the metadata so we remove this torrent from the
        // session so that any other step fails. It will force the user to add
        // again the torrent to the session which will hopefully solve the issue
        // with the metadata.

        kodi::Log(ADDON_LOG_ERROR, "\nTime-out while downloading the metadata. Exiting...");
        kodi::QueueFormattedNotification(QUEUE_ERROR, "%s", kodi::GetLocalizedString(30403).c_str());

        lt_session->remove_torrent(handle);
        // Note: the files associated with the torrent (downloaded files and
        //       .part files) are kept.

        return nullptr;
        // TODO: how to catch this error in python to not run the next commands?
        //       (even when returning nullptr, xbmcvfs.File() will still return
        //       a xbmcvfs.File object)
      }
    };

    // Store the info-hash of the torrent to reuse it in other functions
    // Note: when there is no metadata for a torrent, the handle.info_hash() is
    //       initiliazed with a different info-hash than the actual one. So the
    //       info-hash is stored only when the metadata is downloaded.
    torrent_context->info_hash = handle.info_hash();

    // Generate the JSON string containing all the information that will be
    // returned by the Read() API.
    torrent_context->info_json = generate_info_json(handle);

    return torrent_context;
  }

  // Simply call Open() to have the same behavior whatever the mode used to open
  // the file.
  void* OpenForWrite(const VFSURL& url, bool overWrite) override
  {
    log_function_name(__FUNCTION__);

    return Open(url);
  }

  // The value returned by this method defines the length of the python
  // string returned by xbmcvfs.File(...).read() so it must match the actual
  // length of the data returned by the Read() API.
  // This function is also used in other context (for instance when
  // xbmcvfs.File().seek() is called with 2 as second argument) but in this case
  // the returned value is ignored.
  int64_t GetLength(void* context) override
  {
    log_function_name(__FUNCTION__);

    // Cast the context pointer to access more easily the members
    TorrentContext* torrent_context = static_cast<TorrentContext*>(context);

    int64_t length = torrent_context->info_json.length();
    kodi::Log(ADDON_LOG_DEBUG, "Length of the data is %d", length);

    return length;
  }

  // Return a JSON string containing information about the torrent
  ssize_t Read(void* context, void* buffer, size_t uiBufSize) override
  {
    log_function_name(__FUNCTION__);

    // Cast the context pointer to access more easily the members
    TorrentContext* torrent_context = static_cast<TorrentContext*>(context);

    kodi::Log(ADDON_LOG_DEBUG, "torrent_context->info_json = %s", torrent_context->info_json.c_str());

    // Ensure the size of the data that will be read does not exceed the size of
    // the buffer
    ssize_t length = torrent_context->info_json.length();
    size_t offset = torrent_context->read_data_length;
    if (length > uiBufSize)
    {
      // In case the buffer is smaller than the length of the data to read,
      // read partially the data and store the offset for the next call to the
      // Read() method.
      length = uiBufSize;
      torrent_context->read_data_length += length;
    }

    // Copy the data into the output buffer
    std::memcpy(buffer, torrent_context->info_json.c_str() + offset, length);

    return length;
  }

  // In the context of this add-on, this function is mainly used when
  // xbmcvfs.File().seek() is called so it always return the same value to have
  // a deterministic behavior.
  int64_t GetPosition(void* context) override
  {
    log_function_name(__FUNCTION__);
    return -10000;
  }

  // This function is used to pause/resume the torrent depending on the value of
  // "position". In case the torrent is resumed, it will wait until a chunk of
  // the torrent is downloaded ( position/100 % of the torrent).
  int64_t Seek(void* context, int64_t position, int whence) override
  {
    log_function_name(__FUNCTION__);
    kodi::Log(ADDON_LOG_DEBUG, "position = %d / whence = %d", position, whence);

    if (position < -1 || position > 10000)
    {
      kodi::Log(ADDON_LOG_WARNING, "The seek position %d is not supported so "
                                   "nothing will be done. The only supported "
                                   "values are -1 and [0; 10000].", position);
      // Return -1 so that the error can be handled in the python call
      return -1;
    }

    // Cast the context pointer to access more easily the members
    TorrentContext* torrent_context = static_cast<TorrentContext*>(context);

    // Get the handle of the torrent
    lt::torrent_handle handle = lt_session->find_torrent(torrent_context->info_hash);
    if (!handle.is_valid())
    {
      kodi::Log(ADDON_LOG_ERROR, "Invalid handle found for info-hash %s", torrent_context->info_hash.to_string().c_str());
      kodi::QueueFormattedNotification(QUEUE_ERROR, "%s", kodi::GetLocalizedString(30402).c_str());
      
      // Return -1 so that the error can be handled in the python call
      return -1;
    }

    if (position == -1)
    {
      pause_torrent(handle);
      return 0;
    }
    
    // =========================================================================
    // Here 0 <= position <= 10000 so we resume the torrent and wait until the
    // the requested amount of the torrent is downloaded
    // =========================================================================
    resume_torrent(handle);

    // First check if the initial chunk size is not already downloaded. If it is
    // just exit the function.
    lt::torrent_status status = handle.status();
    if (status.progress*10000 > position)
    {
      kodi::Log(ADDON_LOG_DEBUG, "\nprogress*10000 is %f and the minimum value is %d. Exiting...", status.progress*10000, position);
      return 0;
    }

    // Otherwise we will wait for the initial chunk to be downloaded. While
    // waiting we will:
    //  - log with debug level all the libtorrentalerts
    //  - display a progress dialog box: it will show the user how much remains
    //    it will also allow the user to cancel the wait at anytime. If the user
    //    cancels, the function will exit without pausing the download.

    // List of states as strings for logging: it comes from
    // torrent_status::state_t which is defined in torrent_status.hpp
    std::string states[8] = {
      "queued_for_checking",
      "checking_files",
      "downloading_metadata",
      "downloading",
      "finished",
      "seeding",
      "allocating",
      "checking_resume_data"
    };
    //Initialize some variables
    std::vector<lt::alert*> alerts;

    // Configure and open the dialog box
    kodi::gui::dialogs::CProgress *progress = new kodi::gui::dialogs::CProgress;
    progress->SetHeading(kodi::GetLocalizedString(30400).c_str());
    progress->SetLine(1, kodi::GetLocalizedString(30401).c_str());
    progress->SetCanCancel(true);
    progress->ShowProgressBar(true);
    progress->Open();

    // Update the progress and start the wait loop
    status = handle.status();
    while(status.progress*10000 < position)
    {
      // If the user press the "cancel" button in the dialog, exit without
      // pausing the download.
      if(progress->IsCanceled())
      {
        // Close the dialog box
        progress->Abort();

        break;
      }

      kodi::Log(ADDON_LOG_DEBUG, "\nProgress => %f / State => %s", status.progress*100, states[status.state].c_str());

      // Update the progress bar: the percentage is relative to "position"
      // (not to the whole file)
      progress->SetPercentage(status.progress*10000*100/position);

      lt_session->pop_alerts(&alerts);
      for (lt::alert* a : alerts) {
        kodi::Log(ADDON_LOG_DEBUG, "alert:\n%s", a->message().c_str());
      }

      std::this_thread::sleep_for(std::chrono::seconds(1));

      // Retrieve information about the torrent for the next iteration
      status = handle.status();
    }

    delete progress;

    return 0;
  }

private:

  // Generate a JSON string containing information about the torrent.
  // This JSON will be returned by the Read() API. For now the JSON is built
  // manually as not JSON library is included by default in Kodi source code.
  //
  // The structure is the following:
  //
  //  {
  //    "save_path": string,    <-- path where all the torrents are downloaded
  //    "nb_files": int,        <-- total number of files in the torrent
  //    "files":
  //      [
  //        {
  //          "path": string,   <-- full path of file in the torrent
  //          "name": string,   <-- name of the file (same as path if nb_files is 1)
  //          "size": int       <-- size in bytes of the file
  //        },
  //        {
  //          ...
  //        },
  //        ...
  //      ]
  //  }
  //
  // It must be called only after the metadata of the torrent is downloaded.
  std::string generate_info_json(lt::torrent_handle& handle)
  {
    // Get the path of the folder where the torrents are downloaded
    lt::torrent_status status = handle.status(lt::torrent_handle::query_save_path);

    // Get all the informaiton about the torrent used by the handle
    boost::shared_ptr<const lt::torrent_info> torrent_info = handle.torrent_file();
    int nb_files = torrent_info->num_files();

    // Build the JSON string
    std::string info_json = "{";
    info_json += "\"save_path\": \"" + status.save_path + "\",";
    info_json += "\"nb_files\": " + std::to_string(nb_files) + ",";
    info_json += "\"files\": ";
    info_json += "[ ";
    for (int index=0; index<nb_files; index++)
    {
          info_json += "{ ";
          info_json += "\"path\": \"" + torrent_info->files().file_path(index) + "\",";
          info_json += "\"name\": \"" + torrent_info->files().file_name(index) + "\",";
          info_json += "\"size\": " + std::to_string(torrent_info->files().file_size(index));
          info_json += "},";
    }
    info_json.pop_back(); // remove the last comma
    info_json += "]";
    info_json += "}";

    return info_json;
  }

  // Initialize the libtorrent session if not already done. The session is
  // configured to display alerts only for errors and status changes.
  void initialize_session()
  {
    kodi::Log(ADDON_LOG_DEBUG, "Initializing the libtorrent session...");

    lt::settings_pack pack;
    pack.set_int(lt::settings_pack::alert_mask,
                 lt::alert::status_notification | lt::alert::error_notification);

    lt_session = new lt::session(pack);
  }

  // Pause the torrent associated with the handle by setting the priority of all
  // the files in the torrent to 0 (dont_download)
  void pause_torrent(lt::torrent_handle& handle)
  {
    kodi::Log(ADDON_LOG_DEBUG, "Pausing the torrent...");
    set_all_files_priority(handle, 0);
  }

  // Resume the torrent associated with the handle by setting the priority of
  // all the files in the torrent to 4 (default value)
  void resume_torrent(lt::torrent_handle& handle)
  {
    kodi::Log(ADDON_LOG_DEBUG, "Resuming the torrent...");
    set_all_files_priority(handle, 4);
  }

  // Set the priority of the all the files in the torrent to a specicifc value
  void set_all_files_priority(lt::torrent_handle& handle, int priority)
  {
    // First we retrieve the current priorities so that we get a vector which
    // size is exactly the number of files.
    std::vector<int> priorities = handle.file_priorities();
    // Then we update these priorities
    std::fill(priorities.begin(), priorities.end(), priority);
    handle.prioritize_files(priorities);
  }

};

//----------------------------------------------------------------------

class CMyAddon : public kodi::addon::CAddonBase
{
public:
  CMyAddon() = default;

  ADDON_STATUS CreateInstance(int instanceType,
                              std::string instanceID,
                              KODI_HANDLE instance,
                              KODI_HANDLE& addonInstance) override
  {
    kodi::Log(ADDON_LOG_DEBUG, "Creating an instance of CTorrentFile");
    addonInstance = new CTorrentFile(instance);

    return ADDON_STATUS_OK;
  }
};

ADDONCREATOR(CMyAddon);
