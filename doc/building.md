# Build and installation of the add-on

To build this add-on, the Kodi build chain is used.
It allows to compile the add-on easily, simply providing the dependencies and
Kodi source code.

## Main steps to build the add-on

1. Download Kodi source code with Git (required to compile the add-on)  
  `git clone https://github.com/xbmc/xbmc.git`
1. Use the version of Kodi supported by this add-on  
 `git checkout 18.9-Leia`
1. Declare the add-on in Kodi source code  
    * `mkdir -p xbmc/cmake/addons/addons/vfs.libtorrent`  
    * `echo "vfs.libtorrent https://framagit.org/thombet/vfs.libtorrent main"
          >> xbmc/cmake/addons/addons/vfs.libtorrent/vfs.libtorrent.txt`  
    * `echo "all" >> xbmc/cmake/addons/addons/vfs.libtorrent/platforms.txt`
1. Download the add-on with Git  
  `git clone https://framagit.org/thombet/vfs.libtorrent`
1. Create a folder where the build output will be stored  
    * `mkdir -p vfs.libtorrent/build`  
    * `cd vfs.libtorrent/build`
1. Generate the makefiles  
   `cmake -DADDONS_TO_BUILD=vfs.libtorrent -DADDON_SRC_PREFIX=../..
   -DCMAKE_BUILD_TYPE=Debug
   -DCMAKE_INSTALL_PREFIX=../../xbmc/build/addons -DPACKAGE_ZIP=ON
   -DPACKAGE_DIR=.
   ../../xbmc/cmake/addons`
1. Compile the add-on  
   `make`
1. **OPTIONAL** if the error `No rule to make target "install"` occurs, run
   the command  
   `touch build/boost/src/boost-build/boost-prefix/src/boost-stamp/boost-install`  
   and resume the compilation with `make`

The compiled add-on will be located in `xbmc/build/addons/vfs.libtorrent/`. 

## Installation

There are 2 ways to install the add-on:
1. if you compiled and run Kodi from the source code that was downloaded above,
   there is no additional step: the add-on will be automatically loaded
2. otherwise you may generate an archive of the add-on with the command
   `make package-vfs.libtorrent` and then use the `Install from zip file`
   feature of Kodi (this command will also compile the add-on so it may replace
   the `make` command used above)
 