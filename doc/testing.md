# Test plan

There are no automatic tests so the following use cases must be tested manually
to ensure all the features of the add-on work:
1. Calling `xbmcvfs.File()` with a valid URL does not download any file in the
   download folder
1. Calling `xbmcvfs.File()` with an invalid URL displays an error notification
   in Kodi after 30 seconds
1. Calling `xbmcvfs.File().read()` when the torrent was successfully added
   returns a valid JSON string
1. Calling `xbmcvfs.File().read()` when an error occured at initilization of the
   torrent displays an error notification in Kodi
1. Calling `xbmcvfs.File().write()` downloads the files of the torrent in the
   download folder
1. A progress box is displayed until the first chunk of the torrent is
   downloaded when calling `xbmcvfs.File().write()`
1. The download is paused when `xbmcvfs.File().write()` is called and the
   "abort" button is used in the progress box. The return value must be `False`.
1. Calling `xbmcvfs.File().write()` when an error occured at initilization of the
   torrent displays an error notification in Kodi. The return value must `False`.
1. Calling `xbmcvfs.File().seek(0)` pauses the download of the torrent.
1. Calling `xbmcvfs.File().seek(1)` resumes the download of the torrent.
1. The state of the torrent (pause/downloading) does not change when calling
   `xbmcvfs.File().seek(3)` 
1. Calling `xbmcvfs.File().seek(0)` when an error occured at initilization of the
   torrent displays an error notification in Kodi.
1. The download folder is deleted when Kodi exits if the associated setting is
   enabled